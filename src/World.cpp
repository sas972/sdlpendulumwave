#include <cmath>
#include <functional>
#include <algorithm>

#include "../include/World.h"

World::World()
{
    for( int i = 0; i < 14; ++i)
    {
        // http://www.arborsci.com/cool/pendulum-wave-seems-like-magic-but-its-physics 
        double l = 2*pow(16.0/(16.0+i),2.0);
        m_PendulumList.push_back(new Pendulum(M_PI/25, 10, l));
    }
}

static bool deleteAll( Pendulum* theElement ) { delete theElement; return true; }

World::~World()
{
    m_PendulumList.remove_if(deleteAll);
}

std::vector<Point*> World::calcnext(double t)
{
    m_Positions.clear();
    m_Positions.reserve(m_PendulumList.size());
//    std::transform(m_PendulumList.begin(), m_PendulumList.end(), m_Positions.begin(), bind2nd(std::mem_fun(&Pendulum::calcPosition),t));
    for(std::list<Pendulum*>::iterator i = m_PendulumList.begin(); i != m_PendulumList.end(); ++i)
    {
        Point* tmp = (*i)->calcPosition(t);
        m_Positions.push_back(tmp);
    }
    return m_Positions;
}
