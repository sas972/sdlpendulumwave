#include <cmath>

#include "../include/Pendulum.h"

Pendulum::Pendulum(const double p_alpha0, const double p_g, const double p_l): m_Alpha0(p_alpha0), m_G(p_g),
                                                                                m_L(p_l), m_Ohmega(sqrt(m_G/m_L)), m_Position(0.0,0.0)
{
}

Pendulum::~Pendulum()
{
    //dtor
}

Pendulum::Pendulum(const Pendulum& other)
{
    //copy ctor
}

Point* Pendulum::getPosition()
{
    return &m_Position;
}

Point* Pendulum::calcPosition(double t)
{
    double alpha = m_Alpha0 * cos(m_Ohmega*t);
    m_Position.x = m_L * sin(alpha);
    m_Position.y = m_L * cos(alpha);
    return &m_Position;
}
