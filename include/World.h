#ifndef WORLD_H
#define WORLD_H

#include <list>
#include <vector>

#include "Pendulum.h"


class World
{
    typedef std::list<Pendulum*> pendpl;
    public:
        World();
        virtual ~World();
        std::vector<Point*> calcnext(double t);
    protected:
    private:
        pendpl m_PendulumList;
        std::vector<Point*> m_Positions;
};

#endif // WORLD_H
