#ifndef PENDULUM_H
#define PENDULUM_H


struct Point
{
    double x;
    double y;

    inline Point(double p_x = 0.0, double p_y = 0.0): x(p_x), y(p_y)
    {

    }

    inline Point(const Point& other)
    {
        x = other.x;
        y = other.y;
    }
};

class Pendulum
{
    public:
        Pendulum(const double p_alpha0, const double p_g, const double p_l);
        virtual ~Pendulum();
        Pendulum(const Pendulum& other);
        Point* getPosition();
        Point* calcPosition(double p_t);
    protected:
    private:
         double m_Alpha0;
         double m_G;
         double m_L;
         double m_Ohmega;
         Point m_Position;
};

#endif // PENDULUM_H
